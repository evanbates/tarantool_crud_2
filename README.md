# tarantool_crud_2

This is the code for "Your Basic CRUD: A Simple Tarantool Web App (Tarantool 101, Guide 4B)" on DZone.

Note that you will need to move crud.lua.config into /etc/tarantool/instances.enabled on your server and rename it crud.lua

https://dzone.com/articles/your-basic-crud-a-simple-tarantool-web-app-taranto-1
