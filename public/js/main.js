/* this file goes in usr/share/tarantool/public/js */
let myMap = new Map()

//read
fetch('/read')
.then(response => response.json())
.then(data => {
  mainNode = document.getElementById('div1') 
  for (const x of data) {
    let todo_div = document.createElement('div')
    let todo_text = document.createTextNode(x[1])

    let todo_img = document.createElement('img')
    todo_img.setAttribute('style', 'height: 15px; width: 15px;')
    todo_img.setAttribute('src', 'img/minus-circle.jpg')
    todo_img.setAttribute('onclick', "deleted('" + x[0] + "');")

    let todo_img2 = document.createElement('img')
    todo_img2.setAttribute('style', 'height: 15px; width: 15px;')
    todo_img2.setAttribute('src', 'img/pencil.png')
    todo_img2.setAttribute('onclick', "updated('" + x[0] + "');")

    myMap.set(x[0], x[1])

    todo_div.append(todo_text, todo_img, todo_img2)
    mainNode.appendChild(todo_div)
  }
}).catch((err) => {
   console.log(err)
})

//create
let toDoForm = document.getElementById('todoform')
toDoForm.addEventListener('submit', function(event){
  event.preventDefault()
  let todo = document.getElementById('todoname')
  let data = new URLSearchParams()
  data.append('todovalue', todo.value)
  fetch('/create', {
    method: 'POST',
    body: data
   }).then((response) => {
      location.reload()
   }).catch((err) => {
      console.log(err)
   })
})


//delete
function deleted(delid){
  let url = '/deleted/' + delid
  fetch(url, {
    method: 'GET',
  }).then((response) => {
     location.reload()
  }).catch((err) => {
     console.log(err)
  })
}


//update
function updated(updateid){
  let existingstring = myMap.get(parseInt(updateid))
  let todoname_edit = document.getElementById('todoname_edit')
  todoname_edit.value = existingstring

  let editatodo = document.getElementById('editatodo')
  editatodo.addEventListener('submit', function(event){
    event.preventDefault()
    let data = new URLSearchParams()
    data.append('todoname_edit', todoname_edit.value)
    data.append('todovalue', updateid)
    fetch('/updated', {
      method: 'POST',
      body: data
    }).then((response) => {
       location.reload()
    }).catch((err) => {
       console.log(err)
    })
  })
}
