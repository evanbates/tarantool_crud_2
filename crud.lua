--this is the "operations" crud.lua that goes in usr/share/tarantool

function create(req)
  local selfVar = req:post_param(data)
  for k,v in pairs(selfVar) do
    box.space.crudspace:auto_increment{v}
  end
end

function deleted(self)
  local id = self:stash('id')
  local idx = tonumber(id)
  box.space.crudspace:delete(idx)
end

function updated(req)
  local selfVaredit = req:post_param(data)
  k = tonumber(selfVaredit.todovalue)
  v = selfVaredit.todoname_edit
  box.space.crudspace:update(k, {{'=', 2, v}})
end

function read(self)
  return self:render({ json = box.space.crudspace:select() })
end

function start()
  local server = require('http.server').new(nil, 8080, {app_dir='/usr/share/tarantool'})
  server:route({ path = '/', file='index.html'})
  server:route({ path = '/read'}, read)
  server:route({ path = '/deleted/:id'}, deleted)
  server:route({ path = '/create'}, create)
  server:route({ path = '/updated'}, updated)
  server:start()
end

return {
  start = start
}

